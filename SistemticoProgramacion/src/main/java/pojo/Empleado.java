/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sistemas16
 */
public class Empleado {
    int id;
    String carnet;
    String nombres;
    String apellidos;
    String cedula;
    String direccion;
    String telefono;
    float salarioHora;
    String foto;
    int horasExtras;
    float INSS;
    float IR;
    float salarioNeto;
    private Map<String,Integer> size_map;
    

    public Empleado(int id, String carnet, String nombres, String apellidos, String cedula, String direccion, String telefono, float salarioHora, String foto, int horasExtras, float INSS, float IR, float salarioNeto) {
        this.id = id;
        this.carnet = carnet;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
        this.direccion = direccion;
        this.telefono = telefono;
        this.salarioHora = salarioHora;
        this.foto = foto;
        this.horasExtras = horasExtras;
        this.INSS = INSS;
        this.IR = IR;
        this.salarioNeto = salarioNeto;
    }

    public Empleado(String carnet, String nombres, String apellidos, String cedula, String direccion, String telefono, float salarioHora, String foto, int horasExtras, float INSS, float IR, float salarioNeto) {
        this.carnet = carnet;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
        this.direccion = direccion;
        this.telefono = telefono;
        this.salarioHora = salarioHora;
        this.foto = foto;
        this.horasExtras = horasExtras;
        this.INSS = INSS;
        this.IR = IR;
        this.salarioNeto = salarioNeto;
    }

    public Empleado() {
        sizeMap();
    }
    
    private void sizeMap(){
        size_map = new HashMap<>();
        size_map.put("id", 4);
        size_map.put("carnet", 10);
        size_map.put("nombres", 40);
        size_map.put("apellidos", 40);
        size_map.put("direccion", 100);
        size_map.put("telefono", 20);
        size_map.put("salarioHora", 25);
        size_map.put("foto", 200);
        size_map.put("horasExtras", 200);
        size_map.put("INSS", 200);
        size_map.put("IR", 200);
        size_map.put("salarioNeto", 200);
        
    }

    @Override
    public String toString() {
        return "Empleado{" + "id=" + id + ", carnet=" + carnet + ", nombres=" + nombres + ", apellidos=" + apellidos + ", cedula=" + cedula + ", direccion=" + direccion + ", telefono=" + telefono + ", salarioHora=" + salarioHora + ", foto=" + foto + ", horasExtras=" + horasExtras + ", INSS=" + INSS + ", IR=" + IR + ", salarioNeto=" + salarioNeto + '}';
    }
    
    

}
